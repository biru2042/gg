<?php
namespace App\Repositories\Role;

use App\Permission;
use App\Role;
use Hash;

class RoleRepository implements RoleRepositoryInterface { 

	public function findAll(){
		return Role::paginate(10);
	}

	public function store($data){
		$user = new Permission();
		$user->name = $data['name'];
		$user->email = $data['email'];
		$user->password = $data['password'];
		$user->save();
		
		return $user->id;
	}

	public function update($data,$id){
		$user = Permission::findOrFail($id);
        $user->name = $data['name'];
        $user->email = $data['email'];
        if($data->has('password')){
             $user->password = Hash::make($data['password']);
        }
       
        $user->save();
        return true;
	}
}