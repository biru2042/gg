<?php
namespace App\Repositories\Users;

// use App\Repositories\Users\UserRepositoryInterface;
use App\User;
use Hash;

class UserRepository implements UserRepositoryInterface { 

	public function findAll(){
		return User::paginate(10);
	}

	public function store($data){
		$user = new User();
		$user->name = $data['name'];
		$user->email = $data['email'];
		$user->password = $data['password'];
		$user->save();
		
		return $user->id;
	}

	public function update($data,$id){
		$user = User::findOrFail($id);
        $user->name = $data['name'];
        $user->email = $data['email'];
        if($data->has('password')){
             $user->password = Hash::make($data['password']);
        }
       
        $user->save();
        return true;
	}
}