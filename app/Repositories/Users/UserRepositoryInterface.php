<?php
namespace App\Repositories\Users;

interface UserRepositoryInterface {

	public function findAll();

	public function store($data);

	public function update($data,$id);
}