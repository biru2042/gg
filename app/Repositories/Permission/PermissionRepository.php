<?php
namespace App\Repositories\Permission;

use App\Permission;
use Hash;

class PermissionRepository implements PermissionRepositoryInterface { 

	public function findAll(){
		return Permission::paginate(10);
	}

	public function store($data){
		$user = new Permission();
		$user->name = $data['name'];
		$user->email = $data['email'];
		$user->password = $data['password'];
		$user->save();
		
		return $user->id;
	}

	public function update($data,$id){
		$user = Permission::findOrFail($id);
        $user->name = $data['name'];
        $user->email = $data['email'];
        if($data->has('password')){
             $user->password = Hash::make($data['password']);
        }
       
        $user->save();
        return true;
	}
}