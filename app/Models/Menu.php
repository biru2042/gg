<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\RestroCategory;


class Menu extends Model
{
    protected $fillable = ['resto_id','name', 'price', 'restocategory_id', 'description' ];
    public function category(){
    	return $this->belongsTo(RestroCategory::class,'restocategory_id','id');
    }
}
