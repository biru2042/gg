<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Resturent extends Model
{
    protected $guarded = [];

    public function owner(){
    	return $this->belongsTo(User::class,'owner_id', 'id')->orderBy('name','asc');
    }
}
