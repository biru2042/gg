<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Menu;

class RestroCategory extends Model
{
    

    public function menus(){
    	return $this->hasMany(Menu::class,'restocategory_id','id')->orderBy('name','desc');
    }
}
