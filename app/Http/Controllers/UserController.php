<?php

namespace App\Http\Controllers;

use App\Repositories\Users\UserRepositoryInterface;
use Illuminate\Http\Request;
use App\User;
use App\Role;
use Hash;


class UserController extends Controller
{
    private $userRepo;

    public function __construct(UserRepositoryInterface $userRepo){
        $this->userRepo = $userRepo;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if($request->ajax()){
            return $users = User::get();
        }
        $users = $this->userRepo->findAll();
        return view('manage.users.index', compact("users"));
    }

    public function search(Request $request)
    {
        
        if($search = $request->q){
            $users = User::where(function($query) use($search){
                $query->where('name',"LIKE","%$search%")
                        ->orWhere('email',"LIKE", "%$search%");
            })->get();
        }
        return $users;
        $users = $this->userRepo->findAll();
        return view('manage.users.index', compact("users"));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $roles = Role::all();
        return view('manage.users.create', compact('roles'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        $request->validate([
            'name' => 'required',
            'email' => 'required|email|unique:users',
            'password' => 'required|confirmed',
        ]);
        
        // dd($request->all());

        $user = new User();

        $user->name = $request->name;
        $user->email = $request->email;
        $user->password = Hash::make($request->password);
        $user->save();

        if($request->assigned_roles){
            $user->roles()->sync(explode(",", $request->assigned_roles));
        }

        return redirect()->route("users.show",$user->id);



        $data = $request->all();
        $users = $this->userRepo->store($data);
        if($users){
            // dd($users);
            return redirect()->route("users.show",$users);
        }
        // dd(true);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = User::findOrFail($id);
        return view('manage.users.show',compact('user'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::findOrFail($id);
        $roles = Role::all();
        return view('manage.users.edit',compact('user', 'roles'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        
        $request->validate([
            'name' => 'required',
            'email' => 'required|email',
            'password' => 'nullable|confirmed',
        ]);

        // dd($request->all());
        $user = User::findOrFail($id);
        $user->name = $request->name;
        $user->email = $request->email;

        if($request->customRadio == 'manual'){
            $user->password = $request->password;
        }
        if($request->has('roles')){
            $user->roles()->sync(explode(",",$request->roles));
        }
        // $user->name = $request->name;
        // $user->name = $request->name;
        // $users = $this->userRepo->update($request, $id);

        
        
        return redirect()->route('users.show',$id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
