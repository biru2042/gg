<?php

namespace App\Http\Controllers;

use App\Repositories\Permission\PermissionRepositoryInterface;
use Illuminate\Http\Request;
use App\Permission;
use Session;

class PermissionController extends Controller
{
    private $permissionRepo;

    public function __construct(PermissionRepositoryInterface $permissionRepo){
        $this->permissionRepo = $permissionRepo;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $permissions = $this->permissionRepo->findAll();
        return view('manage.permission.index', compact("permissions"));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('manage.permission.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        if($request->permission_type=="basic"){
            $request->validate([
                'display_name' => 'required|max:255',
                'name' => 'required|max:255|alphadash|unique:permissions,name',
                'description' => 'sometimes|max:255'
            ]);

            $permission = new Permission();
            $permission->name = $request->name;
            $permission->display_name = $request->display_name;
            $permission->description = $request->description;
            $permission->save();

            Session::flash('success', 'Permission has been successfully added');
            return redirect()->route('permissions.index');

            dd($request->all());
        }else{
            $request->validate([
                'resource' => 'required|min:3|max:100|alpha'
            ]);

            $crud = explode(",", $request->crud_selected);
            if(count($crud) > 0){
                    foreach($crud as $k => $x){
                        $slug = strtolower($x)."-".strtolower($request->resource);
                        $display_name = ucwords($x.' '.$request->resource);

                        $description = "Allows a user to " . strtoupper($x) . ' a ' . ucwords($request->resource);

                        $permission = new Permission();
                        $permission->name = $slug;
                        $permission->display_name = $display_name;
                        $permission->description = $description;
                        $permission->save();
                    }

                    Session::flash('success', 'Permissions were all successfully added');
                    return redirect()->route('permissions.index');
            }else{
                return redirect()->route('permissions.create')->withInput();
            }

        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        
        $user = Permission::findOrFail($id);
        return view('manage.permission.show',compact('user'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = Permission::findOrFail($id);
        return view('manage.permission.edit',compact('user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validateWith([
            'display_name' => 'required|max:255',
            'description' => 'sometimes|max:255'
        ]);
      $permission = Permission::findOrFail($id);
      $permission->display_name = $request->display_name;
      $permission->description = $request->description;
      $permission->save();

      Session::flash('success', 'Updated the '. $permission->display_name . ' permission.');
      return redirect()->route('permissions.show', $id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
