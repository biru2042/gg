<?php

namespace App\Http\Controllers\Admin\Resturent;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\RestroCategory;
use App\Models\Menu;
use App\Services\RestoService;

class MenuController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(RestoService $restoService)
    {
        $categories = RestroCategory::with('menus')->get();
        $menus = array();
        foreach($categories as $key => $category){
            // dd($category->menus->count());
            if($category->menus->count() > 0){

                foreach($category->menus as $key => $menu){

                $menus[$category->name][] = array(
                    "name" => $menu->name,
                    "price" => $menu->price,
                    "description" => $menu->description,
                    "restoId" => $menu->resto_id,
               ); 
            }
            }else{
                $menus[$category->name][] = null;
            }            
            
        }

        $restos = $restoService->userRestoAndTables();
        // dd($restos);
        return view('resturent.menu.index', compact('menus', 'restos'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        $postData = $request->validate([
            'restoId' => 'required|numeric',
            'price' => 'required|numeric',
            'name' => 'required|string|unique:menus',
            'description' => 'sometimes|string',
            'category' => 'required|string',
        ]);

        $category = RestroCategory::where('resto_id', $request['restoId'])->where('name', $request['category'])->first();
        $menu = Menu::create([
            'resto_id' => $request['restoId'],
            'name' => $request['name'],
            'price' => $request['price'],
            'description' => $request['description'],
            'restocategory_id' => $category->id,
        ]);

        return response()->json($menu,201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
