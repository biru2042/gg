<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        
        $this->user_register();
        $this->permission_register();
        $this->role_register();
        
    }

    public function user_register(){
       return $this->app->bind('App\Repositories\Users\UserRepositoryInterface',
            'App\Repositories\Users\UserRepository', 
            );
    }

    public function permission_register(){
        return $this->app->bind('App\Repositories\Permission\PermissionRepositoryInterface',
            'App\Repositories\Permission\PermissionRepository', 
            );
    }

    public function role_register(){
        return $this->app->bind('App\Repositories\Role\RoleRepositoryInterface',
            'App\Repositories\Role\RoleRepository', 
            );
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
