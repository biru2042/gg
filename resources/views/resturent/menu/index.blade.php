@extends('layouts.manage')
@section("content")

  <div class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1 class="m-0 text-dark">Manage Roles</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">User
              </li>
            </ol>
            </div>
            <div class="col-sm-6 my-4">
            <a href="" class="btn btn-primary"><i class="fa fa-plus"></i> create</a>
            <a href="" class="btn btn-primary"><i class="fa fa-eye"></i> Show</a>
          </div>
            </div><!-- /.row -->
            </div><!-- /.container-fluid -->
          </div>
          <!-- /.content-header -->
          <!-- Main content -->
          <section class="content">


        <div class="container-fluid">
          <resto-group v-bind:restos="{{json_encode($restos)}}"></resto-group>
        <!-- Small boxes (Stat box) -->
          <menu-container v-bind:menus="{{json_encode($menus)}}"></menu-container>
        </div>


<!-- /.row -->
</section>
@endsection

@section('scripts')
  <script>
    
    var app = new Vue({
      el:"#app",
      data:{},
      methods:{
          deleteMe(){
            alert('dkfjsdl')
          }
      },
    });
  </script>
@endsection