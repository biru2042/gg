@extends('layouts.manage')
@section("content")

    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>User Create</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">General Form</li>
            </ol>
          </div>
          <div class="col-sm-6">
          	@if(session()->has('success'))
          	<div class="alert alert-success" role="alert">
			  {{ session()->get('success') }}
			</div>
			@endif
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
            <!-- general form elements -->
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Create Users @{{permissionsType}}</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form role="form" action="{{ url('manage/permissions') }}" method="post">
              	@csrf
                <div class="card-body">
                  
                  <div class="row">
                    <div class="col-md-6">
                  <div class="form-group">
                        <div class="custom-control custom-radio">
                          <input class="custom-control-input" type="radio" id="basic" name="permission_type" v-model="permissionsType" value="basic">
                          <label for="basic" class="custom-control-label">Basic Permission</label>
                        </div>
                        
                      </div>
                    </div>
                     <div class="col-md-6">
                       <div class="form-group">
                         <div class="custom-control custom-radio">
                          <input class="custom-control-input" type="radio" id="crud" name="permission_type"  v-model="permissionsType" value="crud">
                          <label for="crud" class="custom-control-label">CRUD Permission</label>
                        </div>
                       </div>
                     </div>
                    </div>




                	<div class="form-group" v-if="permissionsType == 'basic' ">
                    <label for="display_name">Name (Display Name)</label>
                    <input type="text" name="display_name" class="form-control" id="display_name" placeholder="Enter display name" value="{{ old('display_name') }}">

                    @error('display_name')
                    <span class="invalid-feedback" style="display: inline-block;" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                  </div>
                  <div class="form-group"  v-if="permissionsType == 'basic' ">
                    <label for="slug">Slug</label>
                    <input type="text" name="name" class="form-control" id="slug" placeholder="Enter slug" {{ old('name') }}>
                    @error('name')
                    <span class="invalid-feedback" style="display: inline-block;" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                  </div>
                  <div class="form-group"  v-if="permissionsType == 'basic' ">
                    <label for="description">Description</label>
                    <input type="text" name="description" class="form-control" id="description" placeholder="Describe what this permission does">
                    @error('description')
                    <span class="invalid-feedback" style="display: inline-block;" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                  </div>


                  <div class="form-group" v-if="permissionsType=='crud' ">

                    <label for="resources">Resources</label>
                    <input type="text" name="resource" class="form-control" id="resources" v-model="resource" placeholder="The name of the resource">
                    @error('resource')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                  </div>
                  <div class="row" v-if="permissionsType=='crud'">
                    <div class="col-md-6">
                      <div class="form-group">
                        <div class="form-group">
                        <div class="custom-control custom-checkbox">
                          <input class="custom-control-input" type="checkbox" id="create" v-model="crudSelected" value="create">
                          <label for="create" class="custom-control-label">Create</label>
                        </div>
                        <div class="custom-control custom-checkbox">
                          <input class="custom-control-input" type="checkbox" id="read" v-model="crudSelected" value="read">
                          <label for="read" class="custom-control-label">Read</label>
                        </div>
                        <div class="custom-control custom-checkbox">
                          <input class="custom-control-input" type="checkbox" id="update" v-model="crudSelected" value="update">
                          <label for="update" class="custom-control-label">Update</label>
                        </div>
                        <div class="custom-control custom-checkbox">
                          <input class="custom-control-input" type="checkbox" id="delete" v-model="crudSelected" value="delete">
                          <label for="delete" class="custom-control-label">Delete</label>
                        </div>
                      </div>
                      </div>
                    </div>
                    <input type="hidden" name="crud_selected" :value="crudSelected">
                    <div class="col-md-6" v-if="permissionsType=='crud' ">
                      <div class="form-group">
                        <table id="example2" class="table table-bordered table-hover dataTable dtr-inline" v-if="resource.length>=3">
                          <thead>
                              <tr role="row">
                                <th>Name</th>
                                <th>Slug</th>
                                <th>Description</th>
                            </thead>
                            <tbody>
                              <tr v-for="item in crudSelected">
                                <td v-text="crudName(item)"></td>
                                <td v-text="crudSlug(item)"></td>
                                <td v-text="crudDescription(item)"></td>
                              </tr>
                            </tbody>
                        </table>
                      </div>
                    </div>
                  </div>



                </div>
                <!-- /.card-body -->

                <div class="card-footer">
                  <button type="submit" class="btn btn-success">Create Permission</button>
                </div>
              </form>
            </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
      
@endsection

@section('scripts')
	<script type="text/javascript">
		var app = new Vue({
			el:"#app",
			data:{
				  permissionsType:'basic',
          resource:'',
          crudSelected:['create','read','update','delete'],
			},
			methods:{
				crudName(item){
					return item.substr(0,1).toUpperCase() + item.substr(1)+" "+this.resource.substr(0,1).toUpperCase() + this.resource.substr(1);
				},
        crudSlug(item){
          return item.toLowerCase() + "-" + this.resource.toLowerCase();
        },
        crudDescription(item){
          return "Allow a User to " + item.toUpperCase() + " a " + app.resource.substr(0,1).toUpperCase() + app.resource.substr(1);
        },
				
			}
		});
	</script>
@endsection