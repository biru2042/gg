@extends('layouts.manage')
@section("content")

    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Edit Permission & Role</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">General Form</li>
            </ol>
          </div>
          <div class="col-sm-6 my-4">
          	<a href="{{ route('permissions.create') }}" class="btn btn-primary"><i class="fa fa-plus"></i> create</a>
          	<a href="{{ route('permissions.show', $user->id) }}" class="btn btn-primary"><i class="fa fa-eye"></i> Show</a>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
            <!-- general form elements -->
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Edit User</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form role="form" action="{{ route('permissions.update',$user->id) }}" method="post">
              	@csrf
              	@method('put')
                <div class="card-body">
                	<div class="form-group">
                    <label for="display_name">Display Name</label>
                    <input type="text" name="display_name" class="form-control" id="display_name" value="{{ $user->display_name }}" v>
                    @error('display_name')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                  </div>
                  <div class="form-group">
                    <label for="slug">Description</label>
                    <input type="text" name="description" class="form-control" id="slug" value="{{ $user->description }}">
                    @error('description')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                  </div>


                </div>
                <!-- /.card-body -->

                <div class="card-footer">
                  <button type="submit" class="btn btn-primary">Submit</button>
                </div>
              </form>
            </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
      
@endsection

@section('scripts')
	<script type="text/javascript">
		var app = new Vue({
			el:"#app",
			data:{
				
			},
			methods:{
				
			}
		});
	</script>
@endsection