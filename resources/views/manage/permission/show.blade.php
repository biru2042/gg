@extends('layouts.manage')
@section("content")

    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>View Permission Details </h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">General Form</li>
            </ol>
          </div>
          <div class="col-sm-6 my-3">
          	<a href="{{ route('permissions.create') }}" class="btn btn-primary"><i class="fa fa-plus"></i> Create User</a>
          	<a href="{{ route('permissions.edit', $user->id) }}" class="btn btn-primary"><i class="fas fa-edit"></i> Edit User</a>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
            <!-- general form elements -->
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">View Permission Detail</h3>
              </div>
              <!-- /.card-header -->
                <div class="card-body">
                	<div class="form-group">
                    <label for="name">Name</label>
                    <p>{{ $user->display_name }}</p>
                    
                    
                  </div>
                  <div class="form-group">
                    <label for="email">Slug</label>
                    <p>{{ $user->name }}</p>
                  </div>

                  <div class="form-group">
                    <label for="email">Description</label>
                    <p>{{ $user->description }}</p>
                  </div>


                </div>
            </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
      
@endsection

@section('scripts')
	<script type="text/javascript">
		var app = new Vue({
			el:"#app",
			data:{

			},
			methods:{
				
			}
		});
	</script>
@endsection