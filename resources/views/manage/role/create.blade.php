@extends('layouts.manage')
@section("content")

    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Create Role</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">General Form</li>
            </ol>
          </div>
          <div class="col-sm-6 my-4">
            <a href="{{ route('roles.index') }}" class="btn btn-primary"><i class="fa fa-eye"></i> Show</a>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
            <!-- general form elements -->

            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Create Users</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form role="form" action="{{ url('manage/roles') }}" method="post">
              	@csrf

                <div class="row">
                  <div class="col-md-6"> 
                    <div class="ml-3">
                    <h1>Role:</h1>
                  </div>
                <div class="card-body">
                  
                  <div class="form-group">
                    <label for="display_name">Name (Display Name)</label>
                    <input type="text" name="display_name" class="form-control" id="display_name" placeholder="Enter display name" value="{{ old('display_name') }}">

                    @error('display_name')
                    <span class="invalid-feedback" style="display: inline-block;" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                  </div>
                  <div class="form-group">
                    <label for="slug">Slug</label>
                    <input type="text" name="name" class="form-control" id="slug" placeholder="Enter slug" {{ old('name') }}>
                    @error('name')
                    <span class="invalid-feedback" style="display: inline-block;" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                  </div>
                  <div class="form-group">
                    <label for="description">Description</label>
                    <input type="text" name="description" class="form-control" id="description" placeholder="Describe what this permission does">
                    @error('description')
                    <span class="invalid-feedback" style="display: inline-block;" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                  </div>
                </div>
                



                </div>

                <div class="col-md-6"> 
                  <div class="ml-3">
                    <h1>Permission:</h1>
                  </div>
                <div class="card-body">
                    <div class="form-group">
                        <label for="display_name">Permission:</label>
                        
                        @foreach($permissions as $key => $permission)  
                        <div class="custom-control custom-checkbox">
                          <input class="custom-control-input" type="checkbox" id="{{ $permission->id }}" v-model="permissionsSelected" :value="{{ $permission->id }}">
                          <label for="{{ $permission->id }}" class="custom-control-label">{{ $permission->display_name }}</label><em class="ml-1">({{$permission->description}})</em>
                        </div>
                        @endforeach
                     <input type="hidden" name="permissions" v-model="permissionsSelected">
                      </div>
                </div>
                </div>



                </div>
                <!-- /.card-body -->

                <div class="card-footer">
                  <button type="submit" class="btn btn-success">Create Permission</button>
                </div>
              </form>
            </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
      
@endsection

@section('scripts')
	<script type="text/javascript">
		var app = new Vue({
			el:"#app",
			data:{
				  permissionsSelected:[],
			},
			methods:{
				
			}
		});
	</script>
@endsection