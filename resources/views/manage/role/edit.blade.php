@extends('layouts.manage')
@section("content")

    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Edit Permission & Role</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">General Form</li>
            </ol>
          </div>
          <div class="col-sm-6 my-4">
          	<a href="{{ route('permissions.create') }}" class="btn btn-primary"><i class="fa fa-plus"></i> create</a>
          	<a href="{{ route('permissions.show', $role->id) }}" class="btn btn-primary"><i class="fa fa-eye"></i> Show</a>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <form role="form" action="{{ route('roles.update',$role->id) }}" method="post">
          @csrf
          @method('put')
        <div class="row">
            <!-- general form elements -->
            <div class="col-md-6">

            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">{{ $role->display_name }}</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              
                <div class="card-body">
                	<div class="form-group">
                    <label for="display_name">Display Name</label>
                    <input type="text" name="display_name" class="form-control" id="display_name" value="{{ $role->display_name }}" v>
                    @error('display_name')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                  </div>
                  <div class="form-group">
                    <label for="display_name">Slug</label>
                    <input type="text" name="name" class="form-control" id="display_name" value="{{ $role->name }}" disabled>
                    @error('display_name')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                  </div>
                  <div class="form-group">
                    <label for="slug">Description</label>
                    <input type="text" name="description" class="form-control" id="slug" value="{{ $role->description }}">
                    @error('description')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                  </div>


                </div>
                <!-- /.card-body -->
            </div>
          </div>
          <div class="col-md-6">
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Permissions:</h3>
              </div>
              <!-- /.card-header -->
              
                <div class="card-body">
                  <div class="form-group">
                    
                  </div>



                  <div class="col-md-6">
                      <div class="form-group">
                        <label for="display_name">Permission:</label>
                        
                        @foreach($permissions as $key => $permission)  
                        <div class="custom-control custom-checkbox">
                          <input class="custom-control-input" type="checkbox" id="{{ $permission->id }}" v-model="permissionsSelected" :value="{{ $permission->id }}">
                          <label for="{{ $permission->id }}" class="custom-control-label">{{ $permission->display_name }}</label><em class="ml-1">({{$permission->description}})</em>
                        </div>
                        @endforeach
                     <input type="hidden" name="permissions" v-model="permissionsSelected">
                      </div>
                    </div>


                </div>
            </div>
          </div>
        <!-- /.row -->
      </div>
      <div>
        <div class="card-footer">
          <button type="submit" class="btn btn-primary">Submit</button>
        </div>
      </div>
    </form>
    </div>
    </section>
      
@endsection

@section('scripts')
	<script type="text/javascript">
		var app = new Vue({
			el:"#app",
			data:{
				  permissionsSelected:{!! $role->permissions->pluck('id') !!}
			},
			methods:{
				
			}
		});
	</script>
@endsection