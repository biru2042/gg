@extends('layouts.manage')
@section("content")

  <div class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1 class="m-0 text-dark">Manage Roles</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">User
              </li>
            </ol>
            </div>
            <div class="col-sm-6 my-4">
            <a href="{{ route('roles.create') }}" class="btn btn-primary"><i class="fa fa-plus"></i> create</a>
            <a href="" class="btn btn-primary"><i class="fa fa-eye"></i> Show</a>
          </div>
            </div><!-- /.row -->
            </div><!-- /.container-fluid -->
          </div>
          <!-- /.content-header -->
          <!-- Main content -->
          <section class="content">


        <div class="container-fluid">
        <!-- Small boxes (Stat box) -->
        <div class="row">
          @foreach($roles as $key => $role)
          <div class="col-lg-5 col-6">
            <!-- small box -->
            <div class="small-box bg-default">
              <div class="inner">
                <h3>{{ $role->display_name }}</h3>

                <p>{{ $role->name }}</p>
                <a href="{{ route('roles.show', $role->id) }}" class="btn btn-primary">Detail</a>
                <a href="{{ route('roles.edit', $role->id) }}" class="btn btn-primary right">Edit</a>
              </div>
              {{-- <div class="icon">
                <i class="ion ion-bag"></i>
              </div> --}}
              {{-- <a href="#" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a> --}}
            </div>
          </div>
          @endforeach
        </div>
      </div>


<!-- /.row -->
</section>
<div class="modal fade" tabindex="-1" role="dialog" id="myModal" ref="vuemodal">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title">Modal title</h4>
        </div>
        <div class="modal-body">
          <p>One fine body&hellip;</p>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <button type="button" class="btn btn-primary">Save changes</button>
        </div>
      </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
  </div><!-- /.modal -->
@endsection

@section('scripts')
  <script>

    Vue.component('hello', {
      template:`<div><h1>hellok xa</h1><button v-on:click="hello" class="btn btn-primary">click me</button></div>`,
      methods:{
        hello(){
          this.$emit("go_to_parent")
        }
      }
    });





    const Alert = {
      template:`<div><h1>Hello k xa</h1></div>`
    };

    
    var app = new Vue({
      el:"#app",
      data:{},
      methods:{
          deleteMe(){
            alert('dkfjsdl')
          }
      },
      components:{
        'alert':Alert,
      }
    });
  </script>
@endsection