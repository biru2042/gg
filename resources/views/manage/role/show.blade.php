@extends('layouts.manage')
@section("content")

    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>{{ $role->display_name }}<span class="ml-3">({{ $role->display_name }})</span></h1>
            <p class="my-3">{{ $role->display_name }}</p>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">General Form</li>
            </ol>
          </div>
          {{-- <div class="col-sm-6 my-3">
          	<a href="{{ route('permissions.create') }}" class="btn btn-primary"><i class="fa fa-plus"></i> Create User</a>
          	<a href="{{ route('permissions.edit', $role->id) }}" class="btn btn-primary"><i class="fas fa-edit"></i> Edit User</a>
          </div> --}}
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
            <!-- general form elements -->
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Permissions:</h3>
              </div>
              <!-- /.card-header -->
                <div class="card-body">
                  <ul>
                  @foreach($role->permissions as $key => $permission)
                	<li>
                    <h4>{{ $permission->display_name }} <em>({{ $permission->description }})</em></h4>
                    <p>{{ $role->display_name }}</p>
                    
                    
                  </li>
                  @endforeach
                  </ul>


                </div>
            </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
      
@endsection

@section('scripts')
	<script type="text/javascript">
		var app = new Vue({
			el:"#app",
			data:{

			},
			methods:{
				
			}
		});
	</script>
@endsection