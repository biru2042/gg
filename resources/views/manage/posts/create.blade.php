@extends('layouts.manage')
@section("content")

    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Create Post</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">General Form</li>
            </ol>
          </div>
          <div class="col-sm-6">
          	@if(session()->has('success'))
          	<div class="alert alert-success" role="alert">
			  {{ session()->get('success') }}
			</div>
			@endif
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
            <!-- general form elements -->
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Create Post</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form role="form" action="{{ url('manage/users') }}" method="post">
              	@csrf
                <div class="row">
                    <div class="col-md-8">
                <div class="card-body">
                  
                	<div class="form-group">
                    <label for="title">Title @{{slug}} </label>
                    <input type="text" name="title" class="form-control" id="title" placeholder="Post Title" value="{{ old('title') }}" v-model="title">
                    @error('title')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                  </div>
                  
                  <slug-widget url="{{ url('/') }}" subdirectory="blog11" :title="title" v-on:slug_changed="updateSlug"></slug-widget>

                  <div class="form-group">
                    <label>Textarea</label>
                    <textarea class="form-control" rows="20" placeholder="Compose Your masterpiece ..."></textarea>
                  </div>






                  <div class="form-group">
                    <label for="email">Email address</label>
                    <input type="email" name="email" class="form-control" id="email" placeholder="Enter email" {{ old('email') }}>
                    @error('email')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                  </div>
                  <div class="form-group">
                    <label for="password">Password</label>
                    <input type="password" name="password" class="form-control" id="password">
                    @error('password')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                  </div>

                  <div class="form-group">
                    <label for="password-confirm">{{ __('Confirm Password') }}</label>
                    <input id="password-confirm" type="password" class="form-control @error('password') is-invalid @enderror" name="password_confirmation" autocomplete="new-password">

                    @error('password')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>


                </div><!-- /.card-body -->
                
              </div><!-- /.card-body -->
              <div class="col-md-4">
                
                <div class="card-body">
                  <div class="form-group">
                    <label for="name">Name</label>
                    <input type="text" name="name" class="form-control" id="name" placeholder="Enter name" value="{{ old('name') }}">
                    @error('name')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                  </div>
                  {{-- <div class="row">
                    <div class="col-md-6">
                      <button>Draft</button>
                    </div>
                    <div class="col-md-6">
                      <button>Draft</button>
                    </div>
                  </div> --}}
                  <div class="form-group">
                    <button class="btn btn-default">Draft Saved</button>
                    <button class="btn btn-primary">Publish</button>
                  </div>
                </div>

              </div>
              </div><!-- end row -->


                <div class="card-footer">
                  <button type="submit" class="btn btn-primary">Submit</button>
                </div>
              </form>
            </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
      
@endsection

@section('scripts')
	<script type="text/javascript">
		var app = new Vue({
			el:"#app",
			data:{
				title:'',
				slug:'',
        api_token:"{{ Auth::user()->api_token }}"
			},
			methods:{
				submitForm(){
					alert("hello kxa");
					axios.get("https://jsonplaceholder.typicode.com/posts")
					.then(res=>{
						console.log(res.data)
					})
				},
        updateSlug(val){
          this.slug=val;
        }
				
			}
		});
	</script>
@endsection