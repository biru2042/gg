@extends('layouts.manage')
@section("content")

  <div class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1 class="m-0 text-dark">Manage Post</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Post</li>
            </ol>
            </div><!-- /.col -->
            </div><!-- /.row -->
            </div><!-- /.container-fluid -->
          </div>
          <!-- /.content-header -->
          <!-- Main content -->
          <section class="content">
            <div class="row">
              <div class="col-12">
                <div class="card">
                  <div class="card-header">
                    <a href="{{ route('posts.create') }}" class="btn btn-primary" title=""><i class="fa fa-plus" aria-hidden="true"></i> Create New Post</a>
                  </div>
                  <!-- /.card-header -->
                  <div class="card-body">
                    <div id="example2_wrapper" class="dataTables_wrapper dt-bootstrap4"><div class="row"><div class="col-sm-12 col-md-6"></div><div class="col-sm-12 col-md-6"></div></div><div class="row"><div class="col-sm-12"><table id="example2" class="table table-bordered table-hover dataTable dtr-inline" role="grid" aria-describedby="example2_info">
                    <thead>
                      <tr role="row">
                        <th>Id</th>
                        <th>Name</th>
                        <th>Email</th>
                        <th>Date Created</th>
                        <th>Action</th></tr>
                    </thead>
                    <tbody>
                      @foreach($posts as $key => $post)
                      {{-- {{dd($post->users)}} --}}
                      <tr role="row" class="{{ ($post->id%2==0) ? 'odd': 'even' }}">
                        <td tabindex="0" class="sorting_1">{{ $post->id }}</td>
                        <td>{{ $post->title }}</td>
                        <td>{{ $post->excerpt }}</td>
                        <td>{{ $post->created_at }}</td>
                        <td>
                        <a class="btn btn-info" href="{{ route('posts.edit', $post->id) }}" title="Edit">Edit</a> 
                        <a class="btn btn-info" href="{{ route('posts.show', $post->id) }}" title="Edit">Show</a>   
                        <a class="btn btn-danger" href="" v-on:click.prevent="deleteMe" title="Delete">Delete</a>
                      </td>
                      </tr>
                      @endforeach
                    </tbody>
                  </table>
                </div>
              </div>
              <div class="row">
                <div class="col-sm-12 col-md-5">
                  <div class="dataTables_info" id="example2_info" role="status" aria-live="polite">Showing 1 to 10 of 57 entries
                  </div>
                </div>
                <div class="col-sm-12 col-md-7">
                  <div class="dataTables_paginate paging_simple_numbers" id="example2_paginate">
                    <ul class="pagination pull-right">
                      <li>{{ $posts->links() }}</li>
                </ul>
              </div>
            </div>
          </div>
        </div>
      </div>
      <!-- /.card-body -->
    </div>
    <!-- /.card -->
  </div>
  <!-- /.col -->
</div>
<!-- /.row -->
</section>
<div class="modal fade" tabindex="-1" role="dialog" id="myModal" ref="vuemodal">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title">Modal title</h4>
        </div>
        <div class="modal-body">
          <p>One fine body&hellip;</p>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <button type="button" class="btn btn-primary">Save changes</button>
        </div>
      </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
  </div><!-- /.modal -->
@endsection

@section('scripts')
  <script>

    Vue.component('hello', {
      template:`<div><h1>hellok xa</h1><button v-on:click="hello" class="btn btn-primary">click me</button></div>`,
      methods:{
        hello(){
          this.$emit("go_to_parent")
        }
      }
    });





    const Alert = {
      template:`<div><h1>Hello k xa</h1></div>`
    };

    
    var app = new Vue({
      el:"#app",
      data:{},
      methods:{
          deleteMe(){
            alert('dkfjsdl')
          }
      },
      components:{
        'alert':Alert,
      }
    });
  </script>
@endsection