@extends('layouts.manage')
@section("content")

    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>User Edit</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">General Form</li>
            </ol>
          </div>
          <div class="col-sm-6 my-4">
          	<a href="{{ route('users.create') }}" class="btn btn-primary"><i class="fa fa-plus"></i> create</a>
          	<a href="{{ route('users.show', $user->id) }}" class="btn btn-primary"><i class="fa fa-eye"></i> Show</a>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
            <!-- general form elements -->
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Edit User</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form role="form" action="{{ route('users.update',$user->id) }}" method="post">
              	@csrf
              	@method('put')
              	<div class="row">
              		<div class="col-md-6">
                <div class="card-body">
                	<div class="form-group">
                    <label for="name">Name</label>
                    <input type="text" name="name" class="form-control" id="name" placeholder="Enter name" value="{{ $user->name }}" v>
                    @error('name')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                  </div>
                  <div class="form-group">
                    <label for="email">Email address</label>
                    <input type="email" name="email" class="form-control" id="email" placeholder="Enter email" value="{{ $user->email }}">
                    @error('email')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                  </div>
                  <div class="form-group">
                        <div class="custom-control custom-radio">
                          <input class="custom-control-input" type="radio" id="customRadio1" name="customRadio" v-model="password_option" value="keep">
                          <label for="customRadio1" class="custom-control-label">Do not change password</label>
                        </div>
                        <div class="custom-control custom-radio">
                          <input class="custom-control-input" type="radio" id="customRadio2" name="customRadio"  v-model="password_option" value="auto">
                          <label for="customRadio2" class="custom-control-label">Auto-Generate New Password</label>
                        </div>
                        <div class="custom-control custom-radio">
                          <input class="custom-control-input mx-3" type="radio" id="customRadio3" name="customRadio" value="manual" v-model="password_option">
                          <label for="customRadio3" class="custom-control-label" >Manually set New Password</label>
                        </div>
                      </div>
                  <div class="form-group" v-if="password_option=='manual'">
                    <label for="password">Password</label>
                    <input type="password" name="password" class="form-control" id="password">
                    @error('password')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                  </div>

                  <div class="form-group" v-if="password_option=='manual'">
                    <label for="password-confirm">{{ __('Confirm Password') }}</label>
                    <input id="password-confirm" type="password" class="form-control @error('password') is-invalid @enderror" name="password_confirmation" autocomplete="new-password">

                    @error('password')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>

				<input type="hidden" name="roles" v-model="rolesSelected">
                </div>
            </div>
                <!-- /.card-body -->
                <div class="col-md-6">
                	<h1>Roles</h1>
                	<div class="form-group">
                        <label for="display_name">Permission:</label>
                        
                        @foreach($roles as $key => $permission)  
                        <div class="custom-control custom-checkbox">
                          <input class="custom-control-input" type="checkbox" id="{{ $permission->id }}" v-model="rolesSelected" :value="{{ $permission->id }}">
                          <label for="{{ $permission->id }}" class="custom-control-label">{{ $permission->display_name }}</label><em class="ml-1">({{$permission->description}})</em>
                        </div>
                        @endforeach
                     
                      </div>
                </div>
				</div>
                <div class="card-footer">
                  <button type="submit" class="btn btn-primary">Submit</button>
                </div>
              </form>
            </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
      
@endsection

@section('scripts')
	<script type="text/javascript">
		var app = new Vue({
			el:"#app",
			data:{
				password_option:'keep',
				rolesSelected:{!! $user->roles->pluck('id') !!}
			},
			methods:{
				submitForm(){
					alert("hello kxa");
					axios.get("https://jsonplaceholder.typicode.com/posts")
					.then(res=>{
						console.log(res.data)
					})
				}
				
			}
		});
	</script>
@endsection