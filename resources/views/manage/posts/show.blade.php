@extends('layouts.manage')
@section("content")
<section class="content-header">
	<div class="container-fluid">
		<div class="row mb-2">
			<div class="col-sm-6">
				<h1>User Details </h1>
			</div>
			<div class="col-sm-6">
				<ol class="breadcrumb float-sm-right">
					<li class="breadcrumb-item"><a href="#">Home</a></li>
					<li class="breadcrumb-item active">General Form</li>
				</ol>
			</div>
			<div class="col-sm-6 my-3">
				<a href="{{ route('users.create') }}" class="btn btn-primary"><i class="fa fa-plus"></i> Create User</a>
				<a href="{{ route('users.edit', $user->id) }}" class="btn btn-primary"><i class="fas fa-edit"></i> Edit User</a>
			</div>
		</div>
	</div><!-- /.container-fluid -->
</section>
<!-- Main content -->
<section class="content">
	<div class="container-fluid">
		<!-- general form elements -->
		<div class="card card-primary">
			<div class="card-header">
				<h3 class="card-title">User Detail</h3>
			</div>
			<!-- /.card-header -->
			<div class="row">
				<div class="col-md-6">
					<div class="card-body">
						<div class="form-group">
							<label for="name">Name</label>
							<p>{{ $user->name }}</p>
							
						</div>
						<div class="form-group">
							<label for="email">Email address</label>
							<p>{{ $user->email }}</p>
						</div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="ml-4">
						<h1>Assigned Roles:</h1>
					</div>
					<div class="card-body">

						<ul>
							<p>{{ $user->roles->count()==0 ? 'This user is not assigned any role' : '' }}</p>
							@foreach($user->roles as $role)

								<li>{{ $role->display_name }} ({{ $role->description }})</li>
							@endforeach
						</ul>
					</div>
				</div>
			</div>
		</div>
		<!-- /.row -->
	</div><!-- /.container-fluid -->
</section>
@endsection
@section('scripts')
<script type="text/javascript">
	var app = new Vue({
		el:"#app",
		data:{
			name:'biru Kunwar',
			email:'biru@gmail.com',
		},
		methods:{
			submitForm(){
				alert("hello kxa");
				axios.get("https://jsonplaceholder.typicode.com/posts")
				.then(res=>{
					console.log(res.data)
				})
			}
			
		}
	});
</script>
@endsection