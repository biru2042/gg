@extends('layouts.manage')
@section("content")

    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>User Create</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">General Form</li>
            </ol>
          </div>
          <div class="col-sm-6">
          	@if(session()->has('success'))
          	<div class="alert alert-success" role="alert">
			  {{ session()->get('success') }}
			</div>
			@endif
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
            <!-- general form elements -->
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Create Users</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form role="form" action="{{ url('manage/users') }}" method="post">
              	@csrf
                  <div class="row">
                    <div class="col-md-6">

                <div class="card-body">
                	<div class="form-group">
                    <label for="name">Name</label>
                    <input type="text" name="name" class="form-control" id="name" placeholder="Enter name" value="{{ old('name') }}">
                    @error('name')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                  </div>
                  <div class="form-group">
                    <label for="email">Email address</label>
                    <input type="email" name="email" class="form-control" id="email" placeholder="Enter email" {{ old('email') }}>
                    @error('email')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                  </div>
                  <div class="form-group">
                    <label for="password">Password</label>
                    <input type="password" name="password" class="form-control" id="password">
                    @error('password')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                  </div>

                  <div class="form-group">
                    <label for="password-confirm">{{ __('Confirm Password') }}</label>
                    <input id="password-confirm" type="password" class="form-control @error('password') is-invalid @enderror" name="password_confirmation" autocomplete="new-password">

                    @error('password')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>


                </div>
                <!-- /.card-body -->


                </div>
                <div class="col-md6">
                  <h1>Available Role</h1>
                  <div class="card-body">
                    <div class="form-group">
                      <label for="display_name">Permission:</label>
                        
                        @foreach($roles as $key => $permission)  
                        <div class="custom-control custom-checkbox">
                          <input class="custom-control-input" type="checkbox" id="{{ $permission->id }}" v-model="rolesSelected" :value="{{ $permission->id }}">
                          <label for="{{ $permission->id }}" class="custom-control-label">{{ $permission->display_name }}</label><em class="ml-1">({{$permission->description}})</em>
                        </div>
                        @endforeach
                        <input type="hidden" name="assigned_roles" v-model="rolesSelected">
                    </div>
                  </div>
                </div>
              </div>







                <div class="card-footer">
                  <button type="submit" class="btn btn-primary">Submit</button>
                </div>
              </form>
            </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
      
@endsection

@section('scripts')
	<script>
		var app = new Vue({
			el:"#app",
			data:{
        rolesSelected:[],
				// rolesSelected:[],
				email:'biru@gmail.com',
			},
			methods:{
				submitForm(){
					alert("hello kxa");
					axios.get("https://jsonplaceholder.typicode.com/posts")
					.then(res=>{
						console.log(res.data)
					})
				}
				
			}
		});
	</script>
@endsection