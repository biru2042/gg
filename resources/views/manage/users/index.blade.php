@extends('layouts.manage')
@section("content")

  <div class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1 class="m-0 text-dark">Manage User</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">User
              </li>
            </ol>
            </div><!-- /.col -->
            </div><!-- /.row -->
            </div><!-- /.container-fluid -->
          </div>
          <!-- /.content-header -->
          <!-- Main content -->
          <section class="content">
            <div class="row">
              <div class="col-12">
                <div class="card">
                  <div class="card-header">
                    <div class="row">
                      <div class="col-md-6">
                        <a href="{{ route('users.create') }}" class="btn btn-primary" title=""><i class="fa fa-plus" aria-hidden="true"></i> Create New User @{{search}}</a>
                        {{-- <user-component></user-component> --}}
                      </div>
                      <div class="col-md-6">
                        <div class="form-group">
                          <input type="text" class="form-control" name="" placeholder="Search Here.." v-model="search" v-on:keyup="searchHit">
                        </div>
                      </div>
                    </div>
                    
                    
                  </div>

                  <!-- /.card-header -->
                  <div class="card-body">
                    <div id="example2_wrapper" class="dataTables_wrapper dt-bootstrap4">
                      <div class="row">
                        <div class="col-sm-12 col-md-6">
                        </div>
                        <div class="col-sm-12 col-md-6">
                        </div>
                      </div>
                      <div class="row">
                        <div class="col-sm-12">

                      <user-component v-bind:search1="search"></user-component>
                </div>
              </div>
              <div class="row">
                <div class="col-sm-12 col-md-5">
                  <div class="dataTables_info" id="example2_info" role="status" aria-live="polite">Showing 1 to 10 of 57 entries
                  </div>
                </div>
                <div class="col-sm-12 col-md-7">
                  <div class="dataTables_paginate paging_simple_numbers" id="example2_paginate">
                    <ul class="pagination pull-right">
                      <li>{{ $users->links() }}</li>
                </ul>
              </div>
            </div>
          </div>
        </div>
      </div>
      <!-- /.card-body -->
    </div>
    <!-- /.card -->
  </div>
  <!-- /.col -->
</div>
<!-- /.row -->
</section>
<div class="modal fade" tabindex="-1" role="dialog" id="myModal" ref="vuemodal">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title">Modal title</h4>
        </div>
        <div class="modal-body">
          <p>One fine body&hellip;</p>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <button type="button" class="btn btn-primary">Save changes</button>
        </div>
      </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
  </div><!-- /.modal -->
@endsection

@section('scripts')
  <script>

    Vue.component('user-component', {
      template:`<div>
      <table id="example2" class="table table-bordered table-hover dataTable dtr-inline">
        <thead>
            <tr role="row">
              <th>@{{search}}</th>
              <th>Id</th>
              <th>Name</th>
              <th>Email</th>
              <th>Date Created</th>
              <th>Action</th></tr>
          </thead>
          <tbody>
          <tr role="row" v-for="(user,index) in users" v-bind:key="index">
              <td tabindex="0" class="sorting_1">@{{user.id}}</td>
              <td>@{{user.name}}</td>
              <td>@{{user.email}}</td>
              <td>@{{user.created_at}}</td>
              <td>
              <a class="btn btn-info" href="" title="Edit">Edit</a> 
              <a class="btn btn-info" href="" title="Edit">Show</a>   
              <a class="btn btn-danger" href="" v-on:click.prevent="deleteMe" title="Delete">Delete</a>
            </td>
            </tr>
            </tbody>
        </table>
      </div>`,
      props:{
        search1:{
          type:String,
          required:false
        }
      },
      data(){
        return{
            users:{},
            search:this.search1
        }
      },
      methods:{
        hello(){
          this.$emit("go_to_parent")
        },
        getSearchData(){
          return this.search1;
        }
        
      },
      watch:{
        search1:_.debounce(function(){
            console.log(this.search)
            axios.get('api/users?q='+this.getSearchData())
                .then((res) => {
                  console.log(res.data);
                  this.users = res.data;
                })
          },600),
      },
      created(){
        axios.get("users")
              .then((res) => {
                // console.log(res.data);
                this.users = res.data;
              })
      }
    });





    const Alert = {
      template:`<div><h1>Hello k xa</h1></div>`
    };

    
    var app = new Vue({
      el:"#app",
      data:{
        search:''
      },
      methods:{
          deleteMe(){
            alert('dkfjsdl')
          },

          searchHit(){
            this.$emit("searchUser",this.search)
          }

          // searchHit:_.debounce(function(){
          //   console.log(this.search)
          //   axios.get('api/users?q='+this.search)
          //       .then((res) => {
          //         console.log(res.data);
          //       })
          // },600),
      },
      // components:{
      //   'alert':Alert,
      // }
    });
  </script>
@endsection