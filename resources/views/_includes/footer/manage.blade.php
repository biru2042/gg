<section class="section-all">
	<div class="container">
		<div class="heading heading-style-2 text-center">
			<h1>Because of our <span class="important">Supporters</span>, we're doing more good for more people</h1>
		</div>
		<div id="supporterCarousel" class="owl-carousel owl-theme supporterCarousel">
			<div class="supporter-item">
				<a href="#" target="_blank">
					<img src="{{ asset('front/images/scranalysis.png') }}" alt="scranalysis">
				</a>
			</div>
			<div class="supporter-item">
				<a href="#" target="_blank">
					<img src="{{ asset('front/images/hope.png') }}" alt="scranalysis">
				</a>
			</div>
			<div class="supporter-item">
				<a href="#" target="_blank">
					<img src="{{ asset('front/images/plum.png') }}" alt="scranalysis">
				</a>
			</div>
			<div class="supporter-item">
				<a href="#" target="_blank">
					<img src="{{ asset('front/images/globalgens.png') }}" alt="scranalysis">
				</a>
			</div>
			<div class="supporter-item">
				<a href="#" target="_blank">
					<img src="{{ asset('front/images/hope.png') }}" alt="scranalysis">
				</a>
			</div>
		</div>
	</div>
</section>
<footer class="site-footer">
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-12 col-lg-8">
				<div class="heading text-center">
					<h3>Subscribe to our Newsletter to Up-to-date</h3>
				</div>
				<div class="subscribe-form-wrap clearfix">
					<form method="post" action="#">
						<div class="form-group">
							<input class="form-control" type="text" placeholder="Full Name">
						</div>
						<div class="form-group">
							<input class="form-control" type="email" placeholder="Email">
						</div>
						<button type="submit">Subscribe</button>
					</form>
				</div>
			</div>
		</div>
		<div class="row d-sm-flex d-lg-flex align-items-center justify-content-center">
			<div class="col-12 col-sm-10 col-md-8 col-lg-5">
				<div class="d-sm-flex d-md-flex align-items-center foot-contact-info">
					<div class="footer-logo">
						<img src="{{ asset('front/images/logo_white.png') }}" alt="logo_white">
					</div>
					<div class="address-wrap">
						<h4>Head Office</h4>
						<address class="foot-address">
							<p>Phone : (+977) 9851081909 )</p>
							<p>Email: info@subbafoundation.com</p>
							<p>28 Manthali, Ramechhap, Nepal</p>
						</address>
						<h4>Contact Office</h4>
						<address class="foot-address">
							<p>Phone : (+977) 9851081909 )</p>
							<p>Email: info@subbafoundation.com</p>
							<p>28 Manthali, Ramechhap, Nepal</p>
						</address>
					</div>
				</div>
			</div>
			<div class="col-12 col-sm-12 col-md-7">
				<div class="row">
					<div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-9">
						<div class="footer-nav">
							<ul>
								<li><a href="#">About us</a></li>
								<li><a href="#">Projects</a></li>
								<li><a href="#">Events</a></li>
								<li><a href="#">Terms & Conditions – Privacy Policy</a></li>
								<li><a href="#">Contact</a></li>
							</ul>
						</div>
					</div>
					<div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-3">
						<div class="social-link">
							<ul>
								<li>
									<a href="#" target="_blank" data-toggle="tooltip" data-placement="top"
										title="Facebook">
										<i class="fab fa-facebook-f"></i>
									</a>
								</li>
								<li>
									<a href="#" target="_blank" data-toggle="tooltip" data-placement="top"
										title="Twitter">
										<i class="fab fa-twitter"></i>
									</a>
								</li>
								<li>
									<a href="#" target="_blank" data-toggle="tooltip" data-placement="top"
										title="Youtube">
										<i class="fab fa-youtube"></i>
									</a>
								</li>
							</ul>
						</div>
					</div>
					<div class="col-12">
						<p class="copyright">
							<span>Copyright © 2020 Protected.</span>
							<span>Design & Develope by :
								<a href="https://www.whitestroke.com" target="_blank"><img
								src="{{ asset('front/images/whitestroke_logo.png') }}" alt="White Stroke"></a></span>
							</p>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- container end -->
	</footer>
	<!-- site-footer end -->