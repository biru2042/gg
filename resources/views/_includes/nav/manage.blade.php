<header id="site-header" class="fixed-top">
	<div class="container-fluid">
		<div class="logo-wrap">
			<a class="logo" href="#">
				<img src="{{ asset('front/images/logo.png') }}" alt="logo">
			</a>
		</div>
		<nav id="site-navigation" class="navbar navbar-expand-lg navbar-dark ">
			<button id="navToggler" class="nav-toggler">
			<span></span>
			<span></span>
			<span></span>
			</button>
			<div id="navbarCollapse" class="navbar-collapse">
				<ul class="navbar-nav">
					<li class="nav-item active">
						<a class="nav-link" href="">
							<i class="fas fa-home"></i>
						</a>
					</li>
					<!-- Dropdown -->
					<li class="nav-item dropdown">
						<a href="" class="nav-link dropdown-toggle">About us</a>
						<span class="dropdown-icon fa fa-plus"></span>
						<ul class="dropdown-menu">
							<li><a class="dropdown-item" href="}">Team</a></li>
						</ul>
					</li>
					<li class="nav-item dropdown">
						<a href="" class="nav-link dropdown-toggle">Projects</a>
						<span class="dropdown-icon fa fa-plus"></span>
						<ul class="dropdown-menu">
							<li><a class="dropdown-item" href="">Entrepreneurship</a></li>
							<li><a class="dropdown-item" href="education.html">Education</a></li>
							<li><a class="dropdown-item" href="health.html">Health</a></li>
						</ul>
					</li>
					<li class="nav-item dropdown">
						<a class="nav-link dropdown-toggle" href="">Events</a>
						<span class="dropdown-icon fa fa-plus"></span>
						<ul class="dropdown-menu">
							<li><a class="dropdown-item" href="">Blood Donation</a></li>
							<li><a class="dropdown-item" href="">Losar Celebration</a></li>
						</ul>
					</li>
					<li class="nav-item">
						<a class="nav-link" href="faq.html">FAQ's</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" href="">Contact</a>
					</li>
					@auth
					<li class="nav-item login">
						<a class="nav-link" href="{{ url('/logout') }}">User Logout</a>
					</li>
					@else
					<li class="nav-item login">
						<a class="nav-link" href="{{ route('login') }}">User Login</a>
					</li>
					@endauth
				</ul>
			</div>
			<div class="navbar-search">
				<button class="search-btn">
				<i class="fas fa-search"></i>
				</button>
				<div class="search-form-wrap">
					<form method="POST" action="#">
						<input type="text" class="form-control" placeholder="Search..">
						<button type="submit"><i class="fas fa-search"></i></button>
					</form>
				</div>
			</div>
		</div>
		<!-- container-fluid end -->
	</nav>
</header>
<!-- site-header end -->