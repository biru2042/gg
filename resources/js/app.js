
require('./bootstrap');
import Vue from 'vue';
import Axios from 'axios';
window.Vue = require('vue');
window.Slug = require('slug');

import _ from 'lodash';
// Slug.defaults.modes ='rfc3986';

import Buefy from 'buefy';

// Vue Modal Plugin
import VModal from 'vue-js-modal'
Vue.use(VModal)

require('vue-multiselect/dist/vue-multiselect.min.css');

Vue.use(Buefy);
// Vue.use(Axios);
import Slug from './components/slug.vue';
Vue.component('slug-widget', Slug);

// Resturent component
import MenuContainer from './modules/menu/MenuContainer';
import Card from './components/Card';
import RestoGroup from './modules/restos/RestoGroup';

// Register Global Components
Vue.component('menu-container', MenuContainer);
Vue.component('card', Card);
Vue.component('resto-group', RestoGroup);



// const app = new Vue({
//     el: '#app',
// });

