$(document).ready(function(){

    $('#state').change(function(e){
    	e.preventDefault();
        var state = $(this).val()
        if(state == ''){
        	return false
        }
        var token = $('meta[name="csrf-token"]').attr('content')
        $.ajax({
        	method:'post',
        	url:"state",
        	data:{state:state, _token: token },
        	success:function(res){
        		$('#district').html(res.state)
        	}
        })
        
    });
  })